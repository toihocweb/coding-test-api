const express = require("express");
const router = express.Router();
const execSync = require('child_process').execSync;
const fs = require("fs")
const CODE_FOLDER = "code";
const path = require("path")

router.post('/', (req, res) => {
    let code = req.body['code']
    const file = "test.js"
    const mainPath = path.join(__dirname, CODE_FOLDER, file)
    const input = {
        input1: [1, 2, 3],
        input2: [2, , 4, 6]
    }
    const expected = {
        input1: 3,
        input2: 6
    }
    try {
        fs.writeFileSync(mainPath, code);
        const proc = execSync("node " + mainPath);
        const regex = new RegExp("\n+", "gi")
        const results = proc.toString().replace(regex, '');

        return res.json({ msg: results });
    } catch (error) {
        console.log("An error occurred");
        console.log(error);
        return res.send(error);
    }
})

module.exports = router