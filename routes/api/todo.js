const express = require("express");
const router = express.Router();
const Todo = require("../../models/Todo");
const io = require("../../socket");

// @route   GET api/todo/test
// @desc    test code route
// @access  Public
router.get("/test", (req, res) => {
  res.status(200).json({ code: 200, msg: "Code Works" });
});

// @route   GET api/todo/list
// @desc    get all todo list
// @access  Public
router.get("/list", (req, res) => {
  Todo.find().sort({ _id: -1 }).then((todos) => res.status(200).json(todos));
});

// @route   POST api/todo/add
// @desc    post new todo
// @access  Public
router.post("/add", (req, res) => {
  const { content } = req.body;
  if (!content) {
    return res.status(400).json({
      error: "Content is not empty",
    });
  }
  const todo = new Todo({ content });

  todo
    .save()
    .then((todo) => res.status(200).json(todo))
    .catch((err) => res.status(500).json({ msg: "Cant not post" }));

  io.getIO().emit("message", { action: "send", content });
});

// @route   DELETE api/todo/id
// @desc    delete  todos id
// @access  Public
router.delete("/:id", (req, res) => {
  const { id } = req.params
  if (id) {
    Todo.deleteOne({ id }).then(todo => res.json({ msg: `deleted ${id}` })).catch(err => res.status(404).json({ msg: 'Not Found' }))
  } else {
    return res.status(500).json({ msg: "Can not delete" })
  }
});

// @route  GET api/todo/id
// @desc    delete  todos id
// @access  Public
router.get("/status/:id", (req, res) => {
  const { id } = req.params
  if (id) {
    Todo.findById(id).then(todo => {
      todo.isDone = !todo.isDone
      todo.save().then(data => res.json(data))
    }).catch(err => res.status(500).json({ msg: "Id Not found" }))
  } else {
    return res.status(400).json({ msg: "Id is not empty" })
  }
});

// @route   DELETE api/todo/list
// @desc    delete all todos
// @access  Public
router.delete("/list", (req, res) => {
  Todo.deleteMany({}).then(res => res.json({ msg: 'deleted all todos' }))
});


module.exports = router;
