const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const path = require("path");
const fs = require("fs");
const todo = require("./routes/api/todo");
const test = require("./routes/api/test");

const app = express();
const cors = require("cors");
// Body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// DB Config
const db = require("./config/keys").mongoURI;
app.use(cors());
// Connect to MongoDB
mongoose
  .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then(() => console.log("MongoDB Connected"))
  .catch((err) => console.log(err));

// Use Routes
// app.use("/api/todo", todo);
app.use("/test", test)
const port = process.env.PORT || 5000;
const server = app.listen(port, () => console.log(`Server  on port ${port}`));
const io = require("./socket").init(server);

io.on("connection", (socket) => {
  console.log("client connected");
});
