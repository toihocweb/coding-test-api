const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Todo Schema
const TodoSchema = new Schema({
  content: {
    type: String,
    required: true,
  },
  isDone: {
    type: Boolean,
    default: false,
  },
  created_date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = Todo = mongoose.model("todos", TodoSchema);
